注意事项

mongodb需要启动副本集模式

需要先修改solr配置 后启动 mongo-connector
==========================================
安装mongo-connector

//pip方式安装
pip install mongo-connector

//git方式安装
git clone https://github.com/mongodb-labs/mongo-connector.git
cd mongo-connector
python setup.py install

------------------------
安装solr-doc-manager
//pip方式安装
pip install solr-doc-manager

//git方式安装
git clone https://github.com/mongodb-labs/solr-doc-manager
cd solr-doc-manager
python setup.py install
========================================

solr配置
---------------
配置solr的core
修改managed-schema
<uniqueKey>id</uniqueKey> 
//修改为 
<uniqueKey>_id</uniqueKey> 


//添加 必须字段
<field name="_id" type="string" indexed="true" stored="true" /> 
<field name="_ts" type="long" indexed="true" stored="true" /> 
<field name="ns" type="string" indexed="true" stored="true"/> 

//注释原有的 
<field name="id" type="string" indexed="true" stored="true" required="true" multiValued="false" />



对于数组的配置
<!-- 数组写法 -->
<field name="tests" type="string" indexed="true" stored="true" multiValued="true"/>
<dynamicField name="tests*" type="text_general" indexed="false" stored="false" />
<copyField source="tests*" dest="tests"/>

---------------
修改solrconfig.xml
//存在则打开注释，不存在添加
<requestHandler name="/admin/luke" class="org.apache.solr.handler.admin.LukeRequestHandler" />
---------------
访问如下地址，存在JSON数据则成功
http://solr_host:8080/solr/core名称/admin/luke?show=schema&wt=json

http://192.168.163.247:8989/solr/ZSK/admin/luke?show=schema&wt=json

==============================================
进入mongo-connector目录
在python3的bin目录下
/usr/local/python3/bin

/usr/local/python3/bi/nmongo-connector --help

./mongo-connector  --auto-commit-interval=0 -m mongo_host:27018 -t http://solr_host:8080/solr/core名称 -d solr_doc_manage


/usr/local/python3/bin/mongo-connector  --auto-commit-interval=0 -m 192.168.163.247:27017 -t http://192.168.163.247:8989/solr/ZSK -d solr_doc_manager
