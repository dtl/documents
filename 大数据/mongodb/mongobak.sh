﻿#!/bin/bash
sourcepath='/app/mongodb-linux-x86_64-2.4.1'/bin
targetpath='/backup/mongobak'
nowtime=$(date +%Y%m%d)
send=`date '+%Y-%m-%d %H:%M:%S'`
 
start()
{
  ${sourcepath}/mongodump --host 127.0.0.1 --port 27017 --out ${targetpath}/${nowtime}
}
execute()
{
  start
  if [ $? -eq 0 ]
  then
    echo "back successfully!"
  else
    echo "back failure!"
  fi
}
 
if [ ! -d "${targetpath}/${nowtime}/" ]
then
 mkdir ${targetpath}/${nowtime}
fi
execute
echo "============== back end ${nowtime} =============="