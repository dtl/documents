﻿

安装 scala 
mkdir /opt/scala
解压 scala 到 /opt/scala 目录下
tar -xzvf scala-2.12.6.tgz -C /opt/scala

--------------------------------------------
设置环境变量
vim /etc/profile

#SCALA set
export SCALA_HOME=/opt/scala/scala-2.12.6
export PATH=$PATH:$SCALA_HOME/bin
#SCALA set

刷新配置文件
source /etc/profile

-------------------------------------------------------

安装spark
mkdir /opt/spark
解压 spark到/opt/spark目录下
tar -xzvf spark-2.3.1-bin-hadoop2.7.tgz -C /opt/spark

--------------------------------------------------------
设置环境变量
vim /etc/profile

#SPARK set
export SPARK_HOME=/opt/spark/spark-2.3.1-bin-hadoop2.7/
export PATH=$PATH:$SPARK_HOME/bin
#SPARK set

刷新配置文件
source /etc/profile

------------------------------------------------------

修改$SPARK_HOME/conf/spark-env.sh，添加如下内容:

cp $SPARK_HOME/conf/spark-env.sh.template $SPARK_HOME/conf/spark-env.sh

创建work目录
mkdir -p /home/spark/work

vim $SPARK_HOME/conf/spark-env.sh


# 配置spark-env.sh
export SPARK_DIST_CLASSPATH=/opt/hadoop/hadoop-2.7.7/bin/hadoop
export JAVA_HOME=/opt/java/jdk1.8.0_171
export SCALA_HOME=/opt/scala/scala-2.12.6
export HADOOP_HOME=/opt/hadoop/hadoop-2.7.7
export HADOOP_CONF_DIR=/opt/hadoop/hadoop-2.7.7/etc/hadoop
export YARN_CONF_DIR=/opt/hadoop/hadoop-2.7.7/etc/hadoop
export SPARK_HOME=/opt/spark/spark-2.3.1-bin-hadoop2.7
export SPARK_MASTER_IP=192.168.173.221 #注意地址
export SPARK_WORKER_CORES=4
export SPARK_WORKER_MEMORY=6g
export SPARK_WORKER_INSTANCES=2

export SPARK_WORKER_DIR=/home/spark/work   #worker目录
#worker自动清理及清理时间间隔
export SPARK_WORKER_OPTS="-Dspark.worker.cleanup.enabled=true -Dspark.worker.cleanup.appDataTtl=604800"


export SPARK_DAEMON_JAVA_OPTS="-Dspark.deploy.recoveryMode=ZOOKEEPER -Dspark.deploy.zookeeper.url=db01:2181,db02:2181,db03:2181 -Dspark.deploy.zookeeper.dir=/spark"

#加载hbase相关jar
export SPARK_CLASSPATH=/opt/hbase/hbase-2.1.0/lib/*

#加载jedis相关jar
#export SPARK_CLASSPATH=$SPARK_CLASSPATH:/home/hadoop/spark-1.6.0-bin-hadoop2.6/lib/jedis/jedis-2.9.0.jar
#export SPARK_CLASSPATH=$SPARK_CLASSPATH:/home/hadoop/spark-1.6.0-bin-hadoop2.6/lib/jedis/commons-pool2-2.4.2.jar


---参数说明---

    JAVA_HOME：Java安装目录
    SCALA_HOME：Scala安装目录
    HADOOP_HOME：hadoop安装目录
    HADOOP_CONF_DIR：hadoop集群的配置文件的目录
    SPARK_MASTER_IP：spark集群的Master节点的ip地址
    SPARK_WORKER_MEMORY：每个worker节点能够最大分配给exectors的内存大小
    SPARK_WORKER_CORES：每个worker节点所占有的CPU核数目
    SPARK_WORKER_INSTANCES：每台机器上开启的worker节点的数目
    SPARK_DAEMON_JAVA_OPTS: ZOOKEEPER集群地址


		SPARK_WORKER_DIR=/home/spark/work                                      #worker目录
    SPARK_WORKER_OPTS="-Dspark.worker.cleanup.enabled=true -Dspark.worker.cleanup.appDataTtl=604800"  #worker自动清理及清理时间间隔
    #history server页面端口、备份数、log日志在HDFS的位置
    SPARK_HISTORY_OPTS="-Dspark.history.ui.port=18080 -Dspark.history.retainedApplications=3 -             Dspark.history.fs.logDirectory=hdfs://10.*.*.41:9000/tmp/spark/applicationHistory"    
    	
    SPARK_LOG_DIR=/opt/data/spark/log            #配置Spark的log日志目录
--------------------- 

--------------------------------------------------------
修改$SPARK_HOME/conf/slaves，添加如下内容：

cp $SPARK_HOME/conf/slaves.template $SPARK_HOME/conf/slaves

vim $SPARK_HOME/conf/slaves
db01
db02
db03
---------------------------------------------------------
将spark 复制到其他节点上
scp -r /opt/spark/spark-2.3.1-bin-hadoop2.7 root@db02:/opt/spark/
scp -r /opt/spark/spark-2.3.1-bin-hadoop2.7 root@db03:/opt/spark/

--------------------------------------------------------

# 将spark目录里jars里面的jar包上传到hdfs，并在配置文件中添加此信息
hdfs dfs -mkdir -p /user/spark_conf/spark_jars/
hdfs dfs -put $SPARK_HOME/jars/* /user/spark_conf/spark_jars/

hdfs dfs -mkdir -p /tmp/spark/eventLogs

# 在$SPARK_HOME/conf/spark-defaults.conf

cp $SPARK_HOME/conf/spark-defaults.conf.template $SPARK_HOME/conf/spark-defaults.conf

vim $SPARK_HOME/conf/spark-defaults.conf
spark.yarn.archive=hdfs:///user/spark_conf/spark_jars/

#两种配置方式 注意结尾的斜杠
#spark.yarn.archive=hdfs://db03:9000/user/spark_conf/spark_jars

# 启动spark history server,需在conf/spark-defaults.conf里添加如下内容(目录/tmp/spark/eventLogs需提前创建好)
spark.yarn.historyServer.address=db01:18080
spark.history.ui.port=18080
spark.eventLog.enabled=true
spark.eventLog.dir=hdfs://db01:9000/tmp/spark/eventLogs
spark.history.fs.logDirectory=hdfs://db01:9000/tmp/spark/eventLogs

#删除spark.serializer注释
spark.serializer                 org.apache.spark.serializer.KryoSerializer

#spark.eventLog.dir 与 spark.history.fs.logDirectory 两个选项的路径必须相同，否则无效。
#设置的路径，比如例中的 /home/hadoop/data/spark/spark-logs ，无论是本地目录，还是 HDFS ，都需要先行创建好在启动应用，如果启动 Spark 时该目录不存在，会报错。

#本地路径存储的方式
spark.eventLog.enabled true
spark.eventLog.dir     file:/home/spark/spark-logs
spark.history.fs.logDirectory file:/home/spark/spark-logs 

------------------------------------------------------------



启动 spark
启动
$SPARK_HOME/sbin/start-all.sh
停止
$SPARK_HOME/sbin/stop-all.sh


启动master进程 如果需要多master可以在其他节点上执行
start-master.sh 

yarn模式启动
spark-shell --master yarn

spark-shell --master yarn-client   #客户端模式连接yarn集群
spark-shell --master yarn-cluster  #集群模式连接yarn集群


spark-shell --master yarn --conf spark.yarn.archive=hdfs:///user/spark_conf/spark_jars/

查看spark
http://db01:8080/

http://db03:8080/

启动history-server
$SPARK_HOME/sbin/start-history-server.sh
查看history-server
http://db01:18080/


--------------------------------------------------------------
启动失败时,注意 $HADOOP_HOME/etc/hadoop/yarn-site.xml 中的配置

	<property>
    <name>yarn.nodemanager.pmem-check-enabled</name>
    <value>false</value>
	</property>
	<property>
	    <name>yarn.nodemanager.vmem-check-enabled</name>
	    <value>false</value>
	</property>
	
	或者
  <property>
        <name>yarn.nodemanager.vmem-check-enabled</name>
        <value>false</value>
        <description>Whether virtual memory limits will be enforced for containers</description>
	</property>
	<property>
        <name>yarn.nodemanager.vmem-pmem-ratio</name>
        <value>4</value>
        <description>Ratio between virtual memory to physical memory when setting memory limits for containers</description>
	</property>    

--------------------------------------------------------------------------
提交任务

$SPARK_HOME/bin/spark-submit  执行的脚本路径
--master spark://db01:7077    spark master节点路径
--executor-memory 2g          执行时的内存大小
--total-executor-cores 4      设置多少核执行

--calss 执行函数main方法地址 jar包路径 可以添加参数1 参数2 ....
--class com.tl.spark.scala.ScalaWordCount /root/spark/spark-test-1.0-SNAPSHOT-shaded.jar hdfs://db01:9000/test/wc hdfs://db01:9000/test/out1./spark-submit --master spark://db01:7077 --executor-memory 2g --total-executor-cores 4 --class com.tl.spark.scala.ScalaWordCount /root/spark/spark-test-1.0-SNAPSHOT-shaded.jar hdfs://db01:9000/test/wc hdfs://db01:9000/test/out1


示例
$SPARK_HOME/bin/spark-submit --master spark://db01:7077 --executor-memory 2g --total-executor-cores 4 --class com.tl.spark.scala.ScalaWordCount /root/spark/spark-test-1.0-SNAPSHOT-shaded.jar hdfs://db01:9000/test/wc hdfs://db01:9000/test/out1
$SPARK_HOME/bin/spark-submit --master yarn --executor-memory 2g --total-executor-cores 4 --class com.tl.spark.scala.ScalaReadHbase /root/spark/spark-test-1.0-SNAPSHOT.jar hdfs://db01:9000/test/wc hdfs://db01:9000/test/out4
$SPARK_HOME/bin/spark-submit --master yarn --executor-memory 2g --total-executor-cores 4 --class com.tl.spark.scala.ScalaDoBulkLoadHbase /root/spark/spark-test-1.0-SNAPSHOT.jar

$SPARK_HOME/bin/spark-submit --master yarn --executor-memory 2g --total-executor-cores 4 --class com.tl.spark.scala.Spark_RO_Hbase /root/spark/spark-test-1.0-SNAPSHOT.jar

$SPARK_HOME/bin/spark-submit --master yarn-client --executor-memory 6g --total-executor-cores 2 --class com.spark.run.SparkCoreDA_BulkLoad /root/spark/spark_DA-1.0.jar /root/spark/conf/conf.yml

$SPARK_HOME/bin/spark-submit --master yarn --num-executors 50 --executor-memory 6g --executor-cores 4 --class com.spark.run.SparkCoreDA_BulkLoad /root/spark/spark_DA-1.0.jar /root/spark/conf/conf.yml

$SPARK_HOME/bin/spark-submit --master yarn --class com.tl.spark.scala.BulkLoadHbase /root/spark/spark-test-1.0-SNAPSHOT.jar

如果执行时 出现错误
java.lang.SecurityException: Invalid signature file digest for Manifest main attributes
说明包里有*.SF *.RSA *.DSA 的签名文件 删除即可

zip -d jar包 'META-INF/*.SF' 'META-INF/*.RSA' 'META-INF/*.DSA'

示例
zip -d spark-test-1.0-SNAPSHOT.jar 'META-INF/*.SF' 'META-INF/*.RSA' 'META-INF/*.DSA'

